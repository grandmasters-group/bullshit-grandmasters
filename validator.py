class Validator:

    @staticmethod
    def knight(origin, dest, board):
        valid_moves = [(1,2), (2,1)]
        x1, y1 = origin
        x2, y2 = dest

        delta = (abs(x2 - x1), abs(y2 - y1))

        if delta not in valid_moves:
            return False

        return True

    @staticmethod
    def queen(origin, dest, board):
        return Validator.rook(origin, dest, board) or Validator.bishop(origin, dest, board)


    @staticmethod
    def king(origin, dest, board):
        valid_moves = [(1, 1), (0, 1), (1, 0)]
        x1, y1 = origin
        x2, y2 = dest

        delta = (abs(x2 - x1), abs(y2 - y1))

        if delta not in valid_moves:
            return False

        return True

    @staticmethod
    def bishop(origin, dest, board):
        x1, y1 = origin
        x2, y2 = dest
        delta = (abs(x2 - x1), abs(y2 - y1))
        return delta[0] == delta[1]

    @staticmethod
    def white_pawn(origin, dest, board):
        x1, y1 = origin
        x2, y2 = dest
        delta = (x2 - x1, y2 - y1)
        if origin[0] in (1,6):
            if delta[0] == 0 and delta[1] in range(0,3):
                return True
        else:
            if delta[0] == 0 and delta[1] in range(0,2):
                return True
        return False

    @staticmethod
    def rook(origin, dest, board):
        x1, y1 = origin
        x2, y2 = dest
        delta0 = abs(x2 - x1)
        delta1 = abs(y2 - y1)
        if delta0 == 0 or delta1 == 0:
            return True
        else:
            return False




if Validator.knight((0,0), (1,2), None) is False:
    print('\x1b[31mKnight: Invalid Move')
else:
    print('\x1b[32mKnight: Valid Move')


if Validator.king((5,7), (4, 7), None) is False:
    print('\x1b[31mKing: Invalid Move')
else:
    print('\x1b[32mKing: Valid Move')






